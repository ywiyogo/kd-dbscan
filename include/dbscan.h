// Copyright (c) 2022 Yongkie Wiyogo
//
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and redistribute the work.
// Full license information available in the project LICENSE file.
//

#ifndef __DBSCAN_H__
#define __DBSCAN_H__

#include <assert.h>
#include <stdio.h>

#include <functional>
#include <memory>
#include <queue>
#include <set>
#include <vector>

#include "kdtree.h"

/**
 * @brief Implementation of the KD-DBSCAN
 *
 * @tparam T an array-like container which represents a n-dimensional data.
 * It defines the operator[] for iteration.
 */
template <typename T>
class DBSCAN {
 public:
  /**
   * @brief Construct a new DBSCAN object
   * @param eps epsilon or radian of a cluster
   * @param min_pts minimal number of points in epsilon radian
   */
  DBSCAN(const float eps, const uint32_t min_pts) : epsilon_(eps), min_pts_(min_pts) {
    assert(min_pts_ > 1 && epsilon_ > 0.f);
  }

  /**
   * @brief Execute the DBSCAN clustering
   *
   * @param dataset dataset input
   * @param dim the dimension of a single data
   * @return true successful run
   * @return false unsuccessful run
   */
  bool Execute(const std::vector<T> &dataset, const size_t dim) {
    assert(dataset.size() > 0 || dim > 0);

    visited_ = std::vector<bool>(dataset.size(), false);
    assigned_ = std::vector<bool>(dataset.size(), false);
    clusters_.clear();
    outliers_.clear();
    datadim_ = dim;

    kdtree_ = std::make_unique<KdTree<T>>(datadim_);
    kdtree_.get()->AddDataset(dataset);

    // Checking the cluster on each index
    for (size_t idx = 0; idx < dataset.size(); ++idx) {
      borderset_.clear();
      if (!visited_[idx]) {
        visited_[idx] = true;
        const std::vector<size_t> neighbors = FindNeigbors(dataset, idx);
        if (neighbors.size() < min_pts_) {
          continue;
        } else {
          auto cluster_idx = clusters_.size();
          clusters_.emplace_back(std::vector<size_t>());
          AddDataIndex(idx);
          AddToCluster(idx, cluster_idx);
          ExpandCluster(dataset, cluster_idx, neighbors);
          // recheck the cluster items
          if (clusters_.at(cluster_idx).size() < min_pts_) {
            clusters_.pop_back();
          }
        }
      }
    }
    // Collect the outliers
    for (size_t idx = 0; idx < dataset.size(); ++idx) {
      if (!assigned_[idx]) {
        outliers_.push_back(idx);
      }
    }
    return true;
  }

  const std::vector<std::vector<size_t>> &GetClusters() const { return clusters_; }

  const std::vector<size_t> &GetOutliers() const { return outliers_; }

 private:
  /**
   * @brief find the neigbors indices
   *
   * @param dataset input
   * @param idx data index of the center
   * @return std::vector<size_t> indices of the neigbours
   */
  std::vector<size_t> FindNeigbors(const std::vector<T> &dataset, const size_t idx) const {
    std::vector<size_t> neighbors;
    assert(kdtree_.get() != nullptr);
    typename KdTree<T>::NearestTree presults = kdtree_.get()->FindNearestNodes(dataset[idx], epsilon_);

    while (!kdtree_.get()->IsEnd(presults)) {
      if (auto data_pos = kdtree_.get()->GetDataPos(presults); data_pos > 0 && idx != static_cast<uint32_t>(data_pos)) {
        neighbors.push_back(data_pos);
      }

      // go to the next entry
      if (!kdtree_.get()->NextTraversal(&presults))
        break;
    }

    return neighbors;
  }
  /**
   * @brief Add a data index to a cluster
   *
   * @param idx input index
   * @param cluster_idx cluster index
   */
  void AddToCluster(const size_t idx, const size_t cluster_idx) {
    clusters_[cluster_idx].push_back(idx);
    assigned_[idx] = true;
  }
  /**
   * @brief Expand a cluster index
   *
   * @param dataset input data
   * @param cluster_idx cluster index
   * @param neighbors vector of neighbours
   */
  void ExpandCluster(const std::vector<T> &dataset, const size_t cluster_idx, const std::vector<size_t> &neighbors) {
    std::queue<uint32_t> q_border;
    for (auto idx : neighbors) q_border.push(idx);
    AddDataIndices(neighbors);

    while (!q_border.empty()) {
      const uint32_t idx = q_border.front();
      q_border.pop();

      if (!visited_[idx]) {
        // Mark if not been visited yet
        visited_[idx] = true;
        const std::vector<size_t> nb_indices = FindNeigbors(dataset, idx);

        // Expanding the neighbors
        if (nb_indices.size() >= min_pts_) {
          AddToCluster(idx, cluster_idx);
          for (uint32_t pidnid : nb_indices) {
            if (!IsInBorderSet(pidnid)) {
              q_border.push(pidnid);
              AddDataIndex(pidnid);
            }
          }
        }
      }
    }
  }

  /**
   * @brief Add data index to the borderset
   *
   * @param idx input of a data index
   */
  void AddDataIndex(const size_t idx) { borderset_.insert(idx); }

  /**
   * @brief Add a vector of indices
   *
   * @param indices input indices
   */
  void AddDataIndices(const std::vector<size_t> &indices) {
    for (size_t idx : indices) this->borderset_.insert(idx);
  }

  /**
   * @brief check if a data index in the borderset
   *
   * @param idx
   * @return true
   * @return false
   */
  bool IsInBorderSet(const size_t idx) const { return this->borderset_.end() != this->borderset_.find(idx); }
  // kd-tree
  std::unique_ptr<KdTree<T>> kdtree_ = nullptr;
  // radius to the neigbour noded in a cluster
  float epsilon_;
  // minimum data or node in a cluster
  uint32_t min_pts_;
  // dimension of the dataset
  size_t datadim_ = 0;
  // list of flags maps to the dataset
  std::vector<bool> visited_;
  std::vector<bool> assigned_;
  std::set<size_t> borderset_;

  /**
   * @brief cluster result
   *
   */
  std::vector<std::vector<size_t>> clusters_;

  /**
   * @brief outliers result
   *
   */
  std::vector<size_t> outliers_;
};

#endif  // __DBSCAN_H__
