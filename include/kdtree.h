// Copyright (c) 2022 Yongkie Wiyogo
//
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and redistribute the work.
// Full license information available in the project LICENSE file.
//

#ifndef _KDTREE_H_
#define _KDTREE_H_

#include <assert.h>
#include <math.h>
#include <stdio.h>

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

/**
 * @brief Implementation of the implicit kd-tree class
 *        See https://en.wikipedia.org/wiki/Implicit_k-d_tree
 *
 * @tparam T an array-like container which represents a n-dimensional data.
 * It defines the operator[] for iteration.
 */
template <typename T>
class KdTree {
 public:
  /**
   * @brief Construct a new Kd Tree <T> object
   *
   * @param dim dimension of the input data
   */
  explicit KdTree<T>(int dim) : dim_(dim) {}

  void print() {
    root_->print(&root_, "");
    if (hyp_rectangle_ && hyp_rectangle_->lower) {
      printf("Rect lower: %f, upper: %f\n", *hyp_rectangle_->lower, *hyp_rectangle_->upper);
    }
  }

  /**
   * @brief A KD-Tree is a binary tree which contains nodes. This class defines a
   * node of the kd-tree.
   *
   */
  class KdNode {
   public:
    KdNode(const T &pdata, const uint32_t pos) : data(pdata), data_pos(pos), direction(0) {}
    explicit KdNode(const T &pdata, const uint32_t pos, uint32_t d) : data(pdata), data_pos(pos), direction(d) {}

    void print(const KdNode &n, std::string indent) {
      printf("%sNode:\n Data: ", indent.c_str());
      for (size_t i = 0; i < n.data.size(); i++) {
        printf("(%s, %f)\n", indent.c_str(), n.data[i]);
      }
      printf("  %sdirection: %u\n", indent.c_str(), direction);

      indent = indent + "  ";
      if (n.left) {
        printf("%sLeft child: \n", indent.c_str());
        print(n.left, indent);
      }
      if (n.right) {
        printf("%sRight child: \n", indent.c_str());
        print(n.right, indent);
      }
      return;
    }
    // Reference of a single data structure
    const T &data;

    uint32_t data_pos;
    // direction of the node in the kd-tree
    uint32_t direction;
    // Left child node
    std::shared_ptr<KdNode> left = nullptr;
    // Right child node
    std::shared_ptr<KdNode> right = nullptr;
  };

  /**
   * @brief  A HyperRectangle is defined as two vectors: upper and lower,
   * with each dimension of lower having the minimum value seen in each
   * dimension, and each dimension of higher having the maximum value seen in
   * each dimension.
   */
  class HyperRectangle {
   public:
    HyperRectangle() : dim(0){};
    HyperRectangle(int dimension, const T &lower_data, const T &upper_data)
        : dim(dimension), lower(lower_data), upper(upper_data){};
    int dim;
    T lower;
    T upper;
  };

  class SearchNode {
   public:
    SearchNode() = default;
    std::shared_ptr<KdNode> node = nullptr;
    float sq_dist = 0.f;
    std::shared_ptr<SearchNode> next = nullptr;
  };

  /**
   * @brief A tree for looking for nearest nodes
   */
  class NearestTree {
   public:
    explicit NearestTree(KdTree<T> &origin) : tree(origin), search_list(std::make_shared<SearchNode>()) {}

    KdTree<T> &tree;
    std::shared_ptr<SearchNode> search_list = nullptr;
    std::shared_ptr<SearchNode> search_iter = nullptr;
    int size = 0;
  };

  /**
   * @brief Insert a data
   *
   * @param data_ptr pointer of the data value
   * @param data pointer of the data structure in the vector
   */
  void InsertData(const size_t pos, const T &data) {
    InsertNode(&root_, pos, data, 0, dim_);
    if (hyp_rectangle_.get() == nullptr) {
      // initialize lower and uper with the first incoming data
      hyp_rectangle_ = std::make_shared<HyperRectangle>(dim_, data, data);
    } else {
      AddDataHypRectangle(*hyp_rectangle_, data);
    }
  }

  /**
   * @brief Add a dataset
   *
   * @param dataset input dataset as vector
   */
  void AddDataset(const std::vector<T> &dataset) {
    for (size_t idx = 0; idx < dataset.size(); ++idx) {
      InsertData(idx, dataset.at(idx));
    }
  }

  /**
   * @brief Find nearest nodes from a given data.
   *
   * @param data n-dimentional data
   * @param range search radius
   * @return NearestTree*
   */
  NearestTree FindNearestNodes(const T &ndata, float range) {
    int ret;
    NearestTree nearest_tree(*this);

    if ((ret = FindNearestAsTree(root_, ndata, range, nearest_tree.search_list)) == -1) {
      return nearest_tree;
    }
    nearest_tree.size = ret;
    RewindTree(nearest_tree);
    return nearest_tree;
  }

  std::vector<T> SortDataByDistance(const T &from, float range) {
    std::vector<T> result;
    KdTree::NearestTree presults = FindNearestNodes(from, range);
    while (!IsEnd(presults)) {
      T data;
      GetData(presults, &data);
      if (from != data) {
        result.push_back(data);
      }

      // go to the next entry
      if (!NextTraversal(&presults))
        break;
    }
    return result;
  }

  /**
   * @brief Treversing the next iterator in the nearest tree
   *
   * @param nearest_tree input tree
   * @return true if an element found
   * @return false if no more element
   */
  bool NextTraversal(NearestTree *nearest_tree) const {
    bool res = false;
    if (nearest_tree->search_iter != nullptr && nearest_tree->search_iter->next != nullptr) {
      nearest_tree->search_iter = nearest_tree->search_iter->next;
      res = true;
    }
    return res;
  }

  /**
   * @brief Get the Data Ptr object
   *
   * @param nearest_tree tree structure of the nearest nodes
   * @param ndata n-dim data result
   * @return void*
   */
  void GetData(const NearestTree &nearest_tree, T *result) const {
    if (nearest_tree.search_iter != nullptr && result != nullptr)
      *result = nearest_tree.search_iter->node->data;
  }

  int32_t GetDataPos(const NearestTree &nearest_tree) const {
    if (nearest_tree.search_iter != nullptr && nearest_tree.search_iter->node != nullptr)
      return nearest_tree.search_iter->node->data_pos;
    else
      return -1;
  }
  /**
   * @brief Check if end node is reached
   *
   * @param nearest_tree
   * @return true if the set iterator reaches the end after the last element
   * @return false if the set iterator doesn't reach the end of the last element
   */
  bool IsEnd(const NearestTree &nearest_tree) const { return nearest_tree.search_iter == nullptr; }

 private:
  /**
   * @brief Insert a new node with n-dimensional data
   *
   * @param node_ptr Pointer of the node pointer
   * @param ndata
   * @param data
   * @param direction
   * @param dim
   */
  void InsertNode(std::shared_ptr<KdNode> *node_ptr, const size_t pos, const T &ndata, uint32_t direction, int dim) {
    assert(node_ptr != nullptr);
    if ((*node_ptr).get() == nullptr) {
      *node_ptr = std::make_shared<KdNode>(ndata, pos, direction);
      return;
    }
    uint32_t dir = (*node_ptr).get()->direction;
    assert(dir < dim_);
    uint32_t next_direction = (dir + 1) % dim;
    if (ndata[dir] < (*node_ptr).get()->data[dir]) {
      return InsertNode(&((*node_ptr).get()->left), pos, ndata, next_direction, dim);
    }
    return InsertNode(&((*node_ptr).get()->right), pos, ndata, next_direction, dim);
  }

  /**
   * @brief find the nearest node
   *
   * @param node input node
   * @param ndata n-dim data to be searched
   * @param range distance range between the node and the n-dim data
   * @param snode list of search node
   * @return int number of neigbors
   */
  int FindNearestAsTree(std::shared_ptr<KdNode> node, const T &ndata, float range,
                        std::shared_ptr<KdTree::SearchNode> snode) {
    float dx = 0.;
    int ret = 0;
    int added_res = 0;

    if (node.get() == nullptr)
      return 0;

    float sq_dist = 0.;
    for (size_t i = 0; i < ndata.size(); i++) {
      float diff = static_cast<float>(node.get()->data[i]) - static_cast<float>(ndata[i]);
      sq_dist += powf(diff, 2);
    }

    if (sq_dist <= powf(range, 2)) {
      if (InsertSearchNode(snode, node, sq_dist)) {
        added_res = 1;
      } else {
        return -1;
      }
    }
    assert(node->direction < dim_);
    dx = ndata[node.get()->direction] - node.get()->data[node.get()->direction];
    ret = FindNearestAsTree(dx <= 0.0 ? node.get()->left : node.get()->right, ndata, range, snode);
    if (ret >= 0 && fabs(dx) < range) {
      added_res += ret;
      ret = FindNearestAsTree(dx <= 0.0 ? node.get()->right : node.get()->left, ndata, range, snode);
    }
    if (ret == -1) {
      return -1;
    }
    added_res += ret;

    return added_res;
  }

  /**
   * @brief Add data to the hyper rectangle
   *
   * @param hrectangle hyper rectangle
   * @param ndata n dimensional data
   */
  void AddDataHypRectangle(HyperRectangle &rect, const T &ndata) const {
    if (ndata < rect.lower) {
      rect.lower = ndata;
    } else {
      rect.upper = ndata;
    }
  }

  /**
   * @brief Insert a node to the search node by linking it
   *
   * @param search_node Target search node
   * @param node input node
   * @param sq_dist square distance
   * @return bool true if a node found
   */
  bool InsertSearchNode(std::shared_ptr<SearchNode> search_node, std::shared_ptr<KdNode> node, float sq_dist) const {
    if (search_node.get() == nullptr)
      return false;
    std::shared_ptr<KdTree::SearchNode> s_node(std::make_shared<KdTree::SearchNode>());

    s_node.get()->node = node;
    s_node.get()->sq_dist = sq_dist;

    if (sq_dist >= 0.0) {
      while (search_node.get()->next != nullptr && search_node.get()->next.get()->sq_dist < sq_dist) {
        search_node = search_node.get()->next;
      }
    }
    s_node.get()->next = search_node.get()->next;
    search_node.get()->next = s_node;
    return true;
  }

  /**
   * @brief Rewind a given nearest tree
   *
   * @param tree
   */
  void RewindTree(NearestTree &tree) const {
    assert(tree.search_list != nullptr);
    tree.search_iter = tree.search_list->next;
  }

  // Root node
  std::shared_ptr<KdNode> root_ = nullptr;

  // Dimension of data
  uint32_t dim_;

  std::shared_ptr<HyperRectangle> hyp_rectangle_ = nullptr;
};

#endif  // _KDTREE_H_
